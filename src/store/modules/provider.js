import BackendService from "@/services/BackendService";
import Vue from 'vue'
import isEmpty from "lodash.isempty";

export const namespaced = true

export const state = () => ({
    provider: {},
    categories: [],
    isSetup: false,
    profileImage:{},
    bookings:[]
})

export const getters = {

    getProvider: (state) => () => {
        return state.provider
    }
}


export const mutations = {

    SET_PROVIDER(state,provider){
        state.provider = provider
    },
    SET_CATEGORIES(state,categories){
        state.categories = categories
    },
    SET_IS_SETUP(state,isSetup){
        state.isSetup = isSetup
    },
    SET_PROFILE(state,images){
        if(!isEmpty(images)){
            state.profileUrl = images.find(image => image.isProfilePicture === true)
        }
    },
    SET_BOOKINGS(state,bookings){
        state.bookings = bookings
    }
}

export const actions = {
    fetchProvider({commit}, providerId){
        BackendService.getProvider(providerId)
            .then(response => {
                let provider = response.data
                if( provider != null){
                    commit('SET_IS_SETUP',true)
                }
                commit('SET_PROVIDER',provider)

            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    createProvider({commit},provider){
        BackendService.createProvider(provider)
            .then(response => {
                commit('SET_PROVIDER',response.data)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    fetchCategories({commit}){
        BackendService.getCategories()
            .then(response => {
                commit('SET_CATEGORIES',response.data.content)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },
    // eslint-disable-next-line no-unused-vars
    postImage({commit},{providerId,file}){
        BackendService.uploadFile(providerId,file)
            .then(response => {
                Vue.$log.info('Image Upload successful' + response)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    updateProvider({commit},provider) {
        BackendService.updateProvider(provider)
            .then(response => {
                commit('SET_PROVIDER',response.data)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    // eslint-disable-next-line no-unused-vars
    postProfileImage({context},{providerId,file}){
        BackendService.uploadProfilePicture(providerId,file)
            .then(response => {
                Vue.$log.info('Image Upload successful' + response)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },
    fetchBookings({commit},providerId){
        BackendService.getProviderBookings(providerId)
            .then(response => {
                commit('SET_BOOKINGS',response.data)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    // eslint-disable-next-line no-unused-vars
    updateBooking({commit}, bookingUpdate){
        BackendService.updateBooking(bookingUpdate)
            // eslint-disable-next-line no-unused-vars
            .then(response => {

            })
            .catch(err => {
                Vue.$log.error(err)
            })
    }
}
