import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import UnderConstruction from "@/views/UnderConstruction";
import Portfolio from "@/views/Portfolio";
import OidcCallback from "@/views/OidcCallback";
import {vuexOidcCreateRouterMiddleware} from 'vuex-oidc'
import store from '@/store'
import BookingsView from "@/views/BookingsView";
import UsersView from "@/views/UsersView";

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
    {
      path: '/portfolio',
      name: 'Portfolio',
      component: Portfolio
    },
    {
      path: '/account',
      name: 'Account',
      component: UnderConstruction
    },
    {
      path: '/bookings',
      name: 'Bookings',
      component: BookingsView
    },
    {
      path: '/oidc-callback', // Needs to match redirectUri (redirect_uri if you use snake case) in you oidcSettings
      name: 'OidcCallback',
      component: OidcCallback
    },
    {
      path: '/users',
      name: 'Users',
      component: UsersView
    }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach(vuexOidcCreateRouterMiddleware(store, 'oidcStore'));
export default router
