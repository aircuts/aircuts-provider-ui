import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueLogger from 'vuejs-logger';
import VCurrencyField from 'v-currency-field'
import { VTextField } from 'vuetify/lib'
import "nprogress/nprogress.css";


const isProduction = process.env.NODE_ENV === 'production';


const options = {
  isEnabled: true,
  logLevel : isProduction ? 'error' : 'debug',
  stringifyArguments : false,
  showLogLevel : true,
  showMethodName : true,
  separator: '|',
  showConsoleColors: true
};

Vue.use(VueLogger, options);
Vue.component('v-text-field', VTextField)
Vue.use(VCurrencyField, {
  // locale: 'pt-BR',
  decimalLength: 2,
  autoDecimalMode: false,
  min: null,
  max: null,
  defaultValue: 0,
  valueAsInteger: false,
  allowNegative: true,
  prefix:'$'
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
