export const oidcSettings = {
    clientId: '1jdeh1vjj3d2oqaj5197ahrgbr',
    redirectUri: window.location.origin + '/oidc-callback',
    responseType: 'code',
    scope: 'openid profile phone email',
    silentRedirectUri: window.location.origin + '/oidc-silent-renew.html',
    automaticSilentRenew: true,
    automaticSilentSignin: false,
    metadata: {
        issuer: 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_QORLyhRci',
        authorization_endpoint: 'https://aircuts.auth.us-east-2.amazoncognito.com/oauth2/authorize',
        userinfo_endpoint: 'https://aircuts.auth.us-east-2.amazoncognito.com/oauth2/userInfo',
        jwks_uri: 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_QORLyhRci/.well-known/jwks.json',
        end_session_endpoint: 'https://aircuts.auth.us-east-2.amazoncognito.com/logout',
        token_endpoint: 'https://aircuts.auth.us-east-2.amazoncognito.com/oauth2/token'
    },
    authority: 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_QORLyhRci/.well-known/openid-configuration'
}
