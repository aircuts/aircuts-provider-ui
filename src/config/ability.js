import { AbilityBuilder, Ability } from '@casl/ability'

export default function defineAbilitiesFor(user){
    let groups  = user['cognito:groups']

    let abilityBuilder = new AbilityBuilder(Ability)

    if(groups.includes('Admin')){
        abilityBuilder.can('manage','User')
    }

    return abilityBuilder.build();
}
