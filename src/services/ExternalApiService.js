import axios from "axios";


export default {

    getSuggestedAddress(query){
        return axios.get('https://autosuggest.search.hereapi.com/v1/geocode',
            {
                params:{
                    apiKey: '6jcuj8nsFezDFmFibqMX3J_9mfzWinucD51az-tdZh8',
                    q: query
                }
            })
    }
}
