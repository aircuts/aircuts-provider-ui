import axios from 'axios'
import FormData from "form-data"
import Nprogress from "nprogress"


const apiClient = axios.create({
    baseURL: 'http://localhost:8081'
})

apiClient.interceptors.request.use(config => {
    Nprogress.start();
    return config;
});

apiClient.interceptors.response.use(response => {
    Nprogress.done();
    return response;
});

export default {

    createProvider(provider){
        return apiClient.post('/providers',
            provider)
    },

    getProvider(providerId){
      return apiClient.get('/providers/' + providerId)
    },

    getCategories(){
        return apiClient.get('/admin/providers/categories')
    },

    uploadFile(providerId, file){
        let formData = new FormData()
        formData.append('file', file)

        return apiClient.post('/providers/' + providerId +'/images',
            formData,
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
    },

    updateProvider(provider){
        return apiClient.patch('/providers/' + provider.id,
            provider)
    },

    uploadProfilePicture(providerId,file){
        let formData = new FormData()
        formData.append('file', file)

        return apiClient.post('/providers/' + providerId +'/profileImage',
            formData,
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
    },

    getProviderBookings(providerId){
        return apiClient.get('/bookings/providers/' + providerId)
    },

    updateBooking(booking){
        return apiClient.patch('/bookings/' + booking.id,
            booking)
    }
}
